﻿using System;
using System.ServiceModel;

namespace chat_praktika_2
{
    public class ServerUser
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public OperationContext operationContext { get; set; }

    }
}

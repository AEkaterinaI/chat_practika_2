﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace chat_praktika_2
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ServiceChat : IServiceChat
    {
        List<ServerUser> users = new List<ServerUser>();
        int Id = 1;

        public int Connect(string name)
        {
            ServerUser user = new ServerUser()
            {
                ID = Id,
                Name = name,
                operationContext = OperationContext.Current
            };
            Id++;
            SendMsg(user.Name + " теперь в чате!",0);
            users.Add(user);
            return user.ID;
        }

        public void Disconnect(int id)
        {
            var user = users.FirstOrDefault(i => i.ID == id);
            if (user!= null)
            {
                users.Remove(user);
                SendMsg(user.Name + " вышел из чата!",0);
            }
        }

        public void SendMsg(string msg, int id)
        {
            foreach (var item in users)
            {
                string answer = DateTime.Now.ToLongTimeString();
                var user = users.FirstOrDefault(i => i.ID == id);
                if (user != null)
                {
                    answer += ": " + user.Name + " ";
                }

                answer += msg;

                item.operationContext.GetCallbackChannel<IServerChatCallBack>().MsgCallBack(answer);

            }
        }
    }
}
